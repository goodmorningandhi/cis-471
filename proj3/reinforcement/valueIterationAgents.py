# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent
import collections, random

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0
        self.runValueIteration()

    def runValueIteration(self):
        # Write value iteration code here
        "*** YOUR CODE HERE ***"
        for i in range(self.iterations):
            
            # Basically, for each iteration, if there is a legal action, find the max.
            mdpStates = self.mdp.getStates()
            valueDict = {}
            # Since self.value is a dict (A Counter)
            for state in mdpStates:
                legalActions = self.mdp.getPossibleActions(state)
                maxValue = 0
                if legalActions:
                    vlist = []
                    for action in legalActions:
                        vlist.append(self.computeQValueFromValues(state, action))
                    maxValue = max(vlist)

                valueDict[state] = maxValue

            for state in mdpStates:
                self.values[state] = valueDict[state]
                # dict with default 0, then update to max value


    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        # dict type, using get() the key
        return self.values.get(state, 0)


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        qValue = 0
        for nextState, prob in self.mdp.getTransitionStatesAndProbs(state, action):
            rsas = self.mdp.getReward(state, action, nextState)
            qValue += prob * (rsas + self.discount * self.getValue(nextState))
            # print("*** computing QValue from values:", nextState, prob, qValue)

        return qValue

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        legalActions = self.mdp.getPossibleActions(state)
        if not legalActions:
            # no legal actions return None
            return None

        vlist = []
        for action in legalActions:
            vlist.append(self.computeQValueFromValues(state, action))
        maxValue = max(vlist)

        inds = [i for i in range(len(vlist)) if vlist[i] == maxValue]
        # indexes list for max values
        action = legalActions[random.choice(inds)]
        # random choice for equal values
        return action

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)

class AsynchronousValueIterationAgent(ValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        An AsynchronousValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs cyclic value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 1000):
        """
          Your cyclic value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy. Each iteration
          updates the value of only one state, which cycles through
          the states list. If the chosen state is terminal, nothing
          happens in that iteration.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state)
              mdp.isTerminal(state)
        """
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"
        mdpStates = self.mdp.getStates()
        # current_iteration = 0
        numS = len(mdpStates)
        for i in range(self.iterations):
            state = mdpStates[i % numS]
            legalActions = self.mdp.getPossibleActions(state)
            maxValue = 0
            if legalActions:
                vlist = []
                for action in legalActions:
                    vlist.append(self.computeQValueFromValues(state, action))
                maxValue = max(vlist)
            self.values[state] = maxValue
            # update values for max value


class PrioritizedSweepingValueIterationAgent(AsynchronousValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A PrioritizedSweepingValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs prioritized sweeping value iteration
        for a given number of iterations using the supplied parameters.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100, theta = 1e-5):
        """
          Your prioritized sweeping value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy.
        """
        self.theta = theta
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"
        mdpStates = self.mdp.getStates()
        pQueue = util.PriorityQueue()
        predecessors = {}

        for state in mdpStates:
            legalActions = self.mdp.getPossibleActions(state)
            maxValue = float('-inf')
            # maxV now is -inf.
            for action in legalActions:
                for nextState, prob in (self.mdp.getTransitionStatesAndProbs(state, action)):
                    if not predecessors.get(nextState):
                        predecessors[nextState] = set()
                        # if predecessors's next doesn't exsist, create an empty one
                    if prob > 0:
                        predecessors[nextState].add(state)
                value = self.computeQValueFromValues(state, action)
                maxValue = value if value > maxValue else maxValue
            if state is not 'TERMINAL_STATE':
                diff = abs(maxValue)
                pQueue.push(state, -diff)


        for i in range(self.iterations):
            # for each p and s do:
            if pQueue.isEmpty():
                return

            state = pQueue.pop()
            if state is not 'TERMINAL_STATE':
                legalActions = self.mdp.getPossibleActions(state)
                maxValue = 0
                if legalActions:
                    vlist = []
                    for action in legalActions:
                        vlist.append(self.computeQValueFromValues(state, action))
                    maxValue = max(vlist)
                self.values[state] = maxValue
                # updated value now

                parents = predecessors[state]
                for parent in parents:
                    legalActions = self.mdp.getPossibleActions(parent)
                    maxValue = 0
                    if legalActions:
                        vlist = []
                        for action in legalActions:
                            vlist.append(self.computeQValueFromValues(parent, action))
                        maxValue = max(vlist)

                    diff = abs(self.values[parent] - maxValue)
                    if diff > self.theta:
                        # using negative cause pQueue is min heap
                        # but want a higher error
                        pQueue.update(parent, -diff)


