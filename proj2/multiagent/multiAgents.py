# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        #print(newGhostStates)
        # --- states vals ---
        currentPos = list(newPos)
        currentFood = currentGameState.getFood().asList()
        distance = -999999
        
        # --- eval ---
        for state in newGhostStates:
            if state.getPosition() == tuple(currentPos) and (newScaredTimes == [0]):
                # if catched return -inf
                return -999999
        
        for x in currentFood:
            manhDistance = -1 * manhattanDistance(currentPos, x)
            # Since dis tend to -infinity, manhattanDistance should be negative
            # then compare to current distance
            distance = manhDistance if manhDistance > distance else distance

        return distance


def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        depth = self.depth
        actions = gameState.getLegalActions(0)
        
        #print(actions)

        if len(actions) > 0:
            # max(pacman), for each action, it calculate the maximum score
            result = max((self.__miniMax(gameState.generateSuccessor(0, action), depth, 1), action) for action in actions)
            return result[1]
        else:
            return self.evaluationFunction(gameState)

    def __miniMax(self, gameState, depth, index):
        '''
        This is the miniMax funtion.
        call in
        '''
        if gameState.isLose() or gameState.isWin() or depth == 0:
        # game over!
            return self.evaluationFunction(gameState)
        
        if index == 0:
            # Pacman is 0, find the Max
            actions = gameState.getLegalActions(0)
            # get legal actions

            if len(actions) > 0:
                return max(self.__miniMax(gameState.generateSuccessor(0, step), depth, 1) for step in actions)
                # find the max
            else:
                return self.evaluationFunction(gameState)
        else:
            # Ghost do the min
            actions = gameState.getLegalActions(index)
            nextIndex = (index + 1) % gameState.getNumAgents()
            # find the next is Pacman or not
            if nextIndex == 0:
                # nextAgent is Pacman, the depth should -1
                depth -= 1
            if len(actions) > 0:
                return min(self.__miniMax(gameState.generateSuccessor(index, step), depth, nextIndex) for step in actions)
                # find the min in this depth
            else:
                return self.evaluationFunction(gameState)
            

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        a = float('-inf')
        # Alpha 
        b = float('inf')
        # Beta
        maxV = float('-inf')
        result = None
        
        for action in gameState.getLegalActions(0):
            # Pacman(0) each action
            v = self.__alphaBetaPruning(gameState.generateSuccessor(0, action), self.depth, 1, a, b)
            # calling a-b pruning
            if v > maxV:
                maxV = v
                result = action
            a = max(a, maxV)
        return result

    def __alphaBetaPruning(self, gameState, depth, index, a, b):
        if depth == 0 or gameState.isLose() or gameState.isWin():
            return self.evaluationFunction(gameState)

        # max's
        elif index == 0:
            # Pacman is 0, find the Max
            maxV = float('-inf')
            actions = gameState.getLegalActions(0)
            # get legal actions
            # pruning
            for action in actions:
                v = self.__alphaBetaPruning(gameState.generateSuccessor(0, action), depth, 1, a, b)
                if v > maxV:
                    maxV = v
                if maxV > b: return maxV
                a = max(a, maxV)
            return maxV

        else:
            # min's
            nextIndex = (index + 1) % gameState.getNumAgents()
            # find the next is Pacman or not
            if nextIndex == 0:
                # nextAgent is Pacman, the depth should -1
                depth -= 1
                
            minV = float('inf')
            actions = gameState.getLegalActions(index)
            # get legal actions
            # pruning
            for action in actions:
                minV = min(minV, self.__alphaBetaPruning(gameState.generateSuccessor(index, action), depth, nextIndex, a, b))
                if minV < a: return minV
                b = min(b, minV)
            return minV

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        maxV = float('-inf')
        result = None
        for action in gameState.getLegalActions(0):
            v = self.__expectiMax(gameState.generateSuccessor(0, action), self.depth, 1)
            if v > maxV:
                maxV = v
                result = action
        return result

    def __expectiMax(self, gameState, depth, index):
        if gameState.isWin() or gameState.isLose() or depth == 0:
            return self.evaluationFunction(gameState)
        if index == 0:
            # find index = 0, max 
            actions = gameState.getLegalActions(0)
            return max(self.__expectiMax(gameState.generateSuccessor(0, action), depth, 1) for action in actions)
        else:
            actions = gameState.getLegalActions(index)
            nextIndex = (index + 1) % gameState.getNumAgents()
            if nextIndex == 0: depth -= 1
            sumExp = sum(self.__expectiMax(gameState.generateSuccessor(index, action), depth, nextIndex) for action in actions)
            # result should be expectation / actions
            return sumExp / len(actions)

def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: basically it calculates the distance between
    the foods and the pacman. return the (current score - minimum cost to
    the destination).

    """
    "*** YOUR CODE HERE ***"
    #util.raiseNotDefined()
    # get current state instead of actions
    position = list(currentGameState.getPacmanPosition())
    # since position is a tuple using list()
    foodPos = currentGameState.getFood().asList()
    # foodPos should be a list of pairs
    foods = []

    for food in foodPos:
        pacmanDist = manhattanDistance(position, food)
        foods.append(pacmanDist)
    #print(currentGameState.getScore())
    return (currentGameState.getScore() - min(foods)) if foods else currentGameState.getScore()


# Abbreviation
better = betterEvaluationFunction
